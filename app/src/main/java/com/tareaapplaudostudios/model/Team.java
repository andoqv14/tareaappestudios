package com.tareaapplaudostudios.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Team implements Parcelable {

    //TODO use parcelable in all you object models
    private final int mTeamImage;
    //TODO use m prefix, don't declare multiple variables in one line
    private final String mTeamTitle;
    private final String mTeamAddress;

    public Team(int teamImage, String teamTitle, String teamAddress) {
        this.mTeamImage = teamImage;
        this.mTeamTitle = teamTitle;
        this.mTeamAddress = teamAddress;
    }

    public int getTeamImage() {
        return mTeamImage;
    }

    public String getTeamTitle() {
        return mTeamTitle;
    }

    public String getTeamAddress() {
        return mTeamAddress;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mTeamImage);
        dest.writeString(this.mTeamTitle);
        dest.writeString(this.mTeamAddress);
    }

    private Team(Parcel in) {
        this.mTeamImage = in.readInt();
        this.mTeamTitle = in.readString();
        this.mTeamAddress = in.readString();
    }

    public static final Parcelable.Creator<Team> CREATOR = new Parcelable.Creator<Team>() {
        @Override
        public Team createFromParcel(Parcel source) {
            return new Team(source);
        }

        @Override
        public Team[] newArray(int size) {
            return new Team[size];
        }
    };
}
