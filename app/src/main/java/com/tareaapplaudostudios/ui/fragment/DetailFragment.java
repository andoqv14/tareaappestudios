package com.tareaapplaudostudios.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tareaapplaudostudios.R;
import com.tareaapplaudostudios.ui.adapter.RecyclerAdapter;

public class DetailFragment extends Fragment implements OnMapReadyCallback {

    //TODO this fragment's view get stuck while moving the map , this behavior is totally unacceptable find the reason and fix it or your homework will be immediately rejected
    //TODO your video view's content does not fill parent fix this behavior before sending your home work

    //TODO USE ONLY ENGLISH IN YOUR CODE , IF YOU USE ENGLISH / SPANISH MIX YOUR CODE WILL BE REJECTED EVERYWHERE
    private static final String KEY_POSITION = "posicion";
    //TODO USER M PREFIX ON PRIVATE FIELDS
    private int currentPosition = -1;
    //TODO USER M PREFIX ON PRIVATE FIELDS
    private GoogleMap mMap;

    //TODO USER M PREFIX ON PRIVATE FIELDS
    //TODO YOU SHOULD GET LATITUDE AND LONGITUDE FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
    //TODO WHY IS THIS FINAL?? DON'T USE METHODS/ MODIFIERS O ANYTHING IN GENERAL IF YOU DON'T KNOW WHAT YOU ARE DOING
    private final LatLng arsenal = new LatLng(51.554888, -0.108438);
    //TODO USER M PREFIX ON PRIVATE FIELDS
    //TODO YOU SHOULD GET LATITUDE AND LONGITUDE FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
    //TODO WHY IS THIS FINAL?? DON'T USE METHODS/ MODIFIERS O ANYTHING IN GENERAL IF YOU DON'T KNOW WHAT YOU ARE DOING
    private final LatLng astonVilla = new LatLng(52.509896, -1.885852);
    //TODO USER M PREFIX ON PRIVATE FIELDS
    //TODO YOU SHOULD GET LATITUDE AND LONGITUDE FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
    //TODO WHY IS THIS FINAL?? DON'T USE METHODS/ MODIFIERS O ANYTHING IN GENERAL IF YOU DON'T KNOW WHAT YOU ARE DOING
    private final LatLng burnley = new LatLng(53.789167, -2.230278);
    //TODO USER M PREFIX ON PRIVATE FIELDS
    //TODO YOU SHOULD GET LATITUDE AND LONGITUDE FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
    //TODO WHY IS THIS FINAL?? DON'T USE METHODS/ MODIFIERS O ANYTHING IN GENERAL IF YOU DON'T KNOW WHAT YOU ARE DOING
    private final LatLng chelsea = new LatLng(51.481667, -0.191111);
    //TODO USER M PREFIX ON PRIVATE FIELDS
    //TODO YOU SHOULD GET LATITUDE AND LONGITUDE FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
    //TODO WHY IS THIS FINAL?? DON'T USE METHODS/ MODIFIERS O ANYTHING IN GENERAL IF YOU DON'T KNOW WHAT YOU ARE DOING
    private final LatLng crystalPalace = new LatLng(51.39828, -0.085485);
    //TODO USER M PREFIX ON PRIVATE FIELDS
    //TODO YOU SHOULD GET LATITUDE AND LONGITUDE FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
    //TODO WHY IS THIS FINAL?? DON'T USE METHODS/ MODIFIERS O ANYTHING IN GENERAL IF YOU DON'T KNOW WHAT YOU ARE DOING
    private final LatLng everton = new LatLng(53.438787, -2.966319);
    //TODO USER M PREFIX ON PRIVATE FIELDS
    //TODO YOU SHOULD GET LATITUDE AND LONGITUDE FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
    //TODO WHY IS THIS FINAL?? DON'T USE METHODS/ MODIFIERS O ANYTHING IN GENERAL IF YOU DON'T KNOW WHAT YOU ARE DOING
    private final LatLng hullCity = new LatLng(53.746522, -0.367692);
    //TODO USER M PREFIX ON PRIVATE FIELDS
    //TODO YOU SHOULD GET LATITUDE AND LONGITUDE FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
    //TODO WHY IS THIS FINAL?? DON'T USE METHODS/ MODIFIERS O ANYTHING IN GENERAL IF YOU DON'T KNOW WHAT YOU ARE DOING
    private final LatLng leicesterCity = new LatLng(52.620044, -1.1415);
    //TODO USER M PREFIX ON PRIVATE FIELDS
    //TODO YOU SHOULD GET LATITUDE AND LONGITUDE FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
    //TODO WHY IS THIS FINAL?? DON'T USE METHODS/ MODIFIERS O ANYTHING IN GENERAL IF YOU DON'T KNOW WHAT YOU ARE DOING
    private final LatLng liverpool = new LatLng(53.430829, -2.96083);
    //TODO USER M PREFIX ON PRIVATE FIELDS
    //TODO YOU SHOULD GET LATITUDE AND LONGITUDE FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
    //TODO WHY IS THIS FINAL?? DON'T USE METHODS/ MODIFIERS O ANYTHING IN GENERAL IF YOU DON'T KNOW WHAT YOU ARE DOING
    private final LatLng manchesterCity = new LatLng(53.483138, -2.200395);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container
            , Bundle savedInstanceState) {

        //TODO WHY ARE YOU DOING THIS , MOST OF THE TIME IF NOT ALWAYS YOU WILL WORK IN TEAM. OTHER DEVELOPER SHOULD UNDERSTAND WHAT YOU ARE TRYING TO ACCOMPLISH
        if (savedInstanceState != null) {
            //TODO can this be converted to a local variable ??
            currentPosition = savedInstanceState
                    .getInt(KEY_POSITION);
        }

        View view = inflater.inflate(R.layout.fragment_detail
                , container, false);
        //TODO USE ONLY ENGLISH IN YOUR CODE , IF YOU USE ENGLISH / SPANISH MIX YOUR CODE WILL BE REJECTED EVERYWHERE
        Toast.makeText(getContext()
                , R.string.mapMessage
                , Toast.LENGTH_LONG).show();

        //TODO USE DESCRIPTIVE VARIABLE NAMES
        ImageView iv_img = (ImageView) view.findViewById(R.id.imgEquipo);
        //TODO USE DESCRIPTIVE VARIABLE NAMES
        VideoView videoView = (VideoView) view.findViewById(R.id.videoEquipo);
        //TODO USE DESCRIPTIVE VARIABLE NAMES
        TextView tv_teamName = (TextView) view.findViewById(R.id.nombreEquipo);
        //TODO USE DESCRIPTIVE VARIABLE NAMES
        TextView tv_teamDescription = (TextView) view.findViewById(R.id.descripcionEquipo);

        MediaController mediaController = new MediaController(getContext());
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        //TODO USE DESCRIPTIVE VARIABLE NAMES
        int imageDetail = 0;
        String teamNameDetail = "";
        String teamDescriptionDetail = "";

        switch (RecyclerAdapter.positionItem) {
            case 0:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                imageDetail = R.drawable.arsenal;
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamNameDetail = getResources().getString(R.string.arsenal);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamDescriptionDetail = getResources().getString(
                        R.string.arsenalDescription);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                videoView.setVideoPath("http://clips.vorwaerts-gmbh.de/VfE_html5.mp4");
                break;
            case 1:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                imageDetail = R.drawable.astonvilla;
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamNameDetail = getResources().getString(R.string.astonvilla);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamDescriptionDetail = getResources().getString(
                        R.string.astonDescription);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                videoView.setVideoPath("http://techslides.com/demos/sample-videos/small.mp4");
                break;
            case 2:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                imageDetail = R.drawable.burnley;
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamNameDetail = getResources().getString(R.string.burnley);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamDescriptionDetail = getResources().getString(
                        R.string.burnleyDescription);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                videoView.setVideoPath("http://clips.vorwaerts-gmbh.de/VfE_html5.mp4");
                break;
            case 3:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                imageDetail = R.drawable.chelsea;
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamNameDetail = getResources().getString(R.string.chelsea);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamDescriptionDetail = getResources().getString(
                        R.string.chelseaDescription);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                videoView.setVideoPath("http://techslides.com/demos/sample-videos/small.mp4");
                break;
            case 4:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                imageDetail = R.drawable.crystalpalace;
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamNameDetail = getResources().getString(R.string.crystalPalace);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamDescriptionDetail = getResources().getString(
                        R.string.crystalDescription);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                videoView.setVideoPath("http://clips.vorwaerts-gmbh.de/VfE_html5.mp4");
                break;
            case 5:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                imageDetail = R.drawable.everton;
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamNameDetail = getResources().getString(R.string.everton);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamDescriptionDetail = getResources().getString(
                        R.string.evertonDescription);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                videoView.setVideoPath("http://techslides.com/demos/sample-videos/small.mp4");
                break;
            case 6:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                imageDetail = R.drawable.hullcity;
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamNameDetail = getResources().getString(R.string.hullCity);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamDescriptionDetail = getResources().getString(
                        R.string.hullDescription);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                videoView.setVideoPath("http://clips.vorwaerts-gmbh.de/VfE_html5.mp4");
                break;
            case 7:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                imageDetail = R.drawable.leicestercity;
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamNameDetail = getResources().getString(R.string.leicesterCity);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamDescriptionDetail = getResources().getString(
                        R.string.leicesterDescription);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                videoView.setVideoPath("http://techslides.com/demos/sample-videos/small.mp4");
                break;
            case 8:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                imageDetail = R.drawable.liverpool;
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamNameDetail = getResources().getString(R.string.liverpool);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamDescriptionDetail = getResources().getString(
                        R.string.liverpoolDescription);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                videoView.setVideoPath("http://clips.vorwaerts-gmbh.de/VfE_html5.mp4");
                break;
            case 9:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                imageDetail = R.drawable.manchestercity;
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamNameDetail = getResources().getString(R.string.manchesterCity);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                teamDescriptionDetail = getResources().getString(
                        R.string.manchesterDescription);
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                videoView.setVideoPath("http://techslides.com/demos/sample-videos/small.mp4");
                break;
        }

        //TODO USE DESCRIPTIVE VARIABLE NAMES
        iv_img.setImageDrawable(ContextCompat.getDrawable(getContext()
                , imageDetail));
        //TODO USE DESCRIPTIVE VARIABLE NAMES
        tv_teamName.setText(teamNameDetail);
        //TODO USE DESCRIPTIVE VARIABLE NAMES
        tv_teamDescription.setText(teamDescriptionDetail);
        videoView.start();

        //TODO set title to support action bar / toolbar not activity
        getActivity().setTitle(R.string.detalle);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment)
                getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //TODO here is totally unnecessary to have a field called google maps you can pass
        //TODO it as a parameter to your methods, extra field instances mean more allocated memory if you can avoid it do it

        switch (RecyclerAdapter.positionItem) {
            case 0:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                addMarker(arsenal, getResources().getString(R.string.arsenal));
                moveCameraMarker(arsenal);
                break;
            case 1:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                addMarker(astonVilla, getResources().getString(R.string.astonvilla));
                moveCameraMarker(astonVilla);
                break;
            case 2:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                addMarker(burnley, getResources().getString(R.string.burnley));
                moveCameraMarker(burnley);
                break;
            case 3:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                addMarker(chelsea, getResources().getString(R.string.chelsea));
                moveCameraMarker(chelsea);
                break;
            case 4:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                addMarker(crystalPalace, getResources().getString(R.string.crystalPalace));
                moveCameraMarker(crystalPalace);
                break;
            case 5:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                addMarker(everton, getResources().getString(R.string.everton));
                moveCameraMarker(everton);
                break;
            case 6:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                addMarker(hullCity, getResources().getString(R.string.hullCity));
                moveCameraMarker(hullCity);
                break;
            case 7:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                addMarker(leicesterCity, getResources().getString(R.string.leicesterCity));
                moveCameraMarker(leicesterCity);
                break;
            case 8:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                addMarker(liverpool, getResources().getString(R.string.liverpool));
                moveCameraMarker(liverpool);
                break;
            case 9:
                //TODO YOU SHOULD GET THIS VALUES FROM JSON FEED HARD CODED VALUES ARE UNACCEPTABLE
                addMarker(manchesterCity, getResources().getString(R.string.manchesterCity));
                moveCameraMarker(manchesterCity);
                break;
        }
    }

    //TODO use javadoc in all your methods
    private void moveCameraMarker(LatLng latLng) {
        mMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(latLng, 10));
    }

    //TODO use javadoc in all your methods
    private void addMarker(LatLng latLng, String title) {
        mMap.addMarker(new MarkerOptions()
                .position(latLng)).setTitle(title);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_POSITION, currentPosition);
    }

    @Override
    public void onResume() {
        super.onResume();
        //TODO set title to support action bar / toolbar not activity
        getActivity().setTitle(R.string.detalle);
    }

}
