package com.tareaapplaudostudios.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tareaapplaudostudios.R;
import com.tareaapplaudostudios.model.Team;
import com.tareaapplaudostudios.ui.activity.MainActivity;
import com.tareaapplaudostudios.ui.adapter.RecyclerAdapter;

import java.util.ArrayList;

public class TeamListFragment extends Fragment {

    //TODO USE RETROFIT LIBRARY FOR DOWNLOADING DATA THIS IS UNACCEPTABLE !!
    //TODO use json data feed using hard coded values is unacceptable
    public RecyclerAdapter loadDataWithAdapter() {
        final int[] images = {
                R.drawable.arsenal, R.drawable.astonvilla
                , R.drawable.burnley, R.drawable.chelsea
                , R.drawable.crystalpalace, R.drawable.everton
                , R.drawable.hullcity, R.drawable.leicestercity
                , R.drawable.liverpool, R.drawable.manchestercity};
        final String[] names = getActivity().getResources()
                .getStringArray(R.array.lista_equipos);
        final String[] addresses = getActivity().getResources()
                .getStringArray(R.array.direccion_equipos);

        ArrayList<Team> teamArrayList = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            teamArrayList.add(new Team(images[i], names[i], addresses[i]));
        }

        return new RecyclerAdapter(getContext(), teamArrayList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater
            , ViewGroup container
            , Bundle savedInstanceState) {

        View rooView = inflater.inflate(R.layout.fragment_teamlist, container, false);
        //TODO use descriptive object names
        RecyclerView recyclerView = (RecyclerView) rooView.findViewById(R.id.recyclerView);
        //TODO why are you setting this property?
        recyclerView.setHasFixedSize(true);
        //TODO use descriptive object names
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(loadDataWithAdapter());

        //TODO USE RETROFIT LIBRARY FOR DOWNLOADING DATA THIS IS UNACCEPTABLE !!
        //TODO use json data feed using hard coded values is unacceptable
        return rooView;
    }

    //TODO set activity title on view created
    @Override
    public void onResume() {
        super.onResume();
        //TODO why are you doing this?? re think your logic to avoid this unnecessary activity calls
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            //TODO don't call activity methods from a fragment use an interface or rethink your logic
            activity.hideUpButton();
        }
        //TODO set title to support action bar / toolbar not activity
        getActivity().setTitle(R.string.tituloTarea);
    }

}