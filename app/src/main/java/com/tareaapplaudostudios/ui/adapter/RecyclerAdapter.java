package com.tareaapplaudostudios.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tareaapplaudostudios.R;
import com.tareaapplaudostudios.model.Team;
import com.tareaapplaudostudios.ui.activity.DetailActivity;

import java.util.ArrayList;
//TODO read Google's java guidelines before breaking up declarations like this
public class RecyclerAdapter extends RecyclerView.Adapter<
        RecyclerAdapter.TeamViewHolder> {

    //TODO use m prefix for private variables
    //TODO why is context final?? do you know what final means?? don't use things you don't know what are for
    private final Context context;
    //TODO use m prefix for private variables
    //TODO why is this list final?? do you know what final means?? don't use things you don't know what are for
    private final ArrayList<Team> teamList;
    //TODO why is this int static?? do you know what static means?? don't use things you don't know what are for
    //TODO use m prefix for private variables
    public static int positionItem;

    //TODO read Google's java guidelines before breaking up declarations like this
    public RecyclerAdapter(Context context
            , ArrayList<Team> teamList) {
        //TODO don't use this keyword is unnecessary
        this.context = context;
        //TODO don't use this keyword is unnecessary
        this.teamList = teamList;
    }

    @Override
    public TeamViewHolder onCreateViewHolder(ViewGroup parent
            , int viewType) {
        //TODO inline unnecessary object declarations
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rv_list, parent, false);

        return new TeamViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TeamViewHolder holder, int position) {
        Team team = teamList.get(position);

        //TODO USE GLIDE LIBRARY FOR IMAGE LOADING THIS IS UNACCEPTABLE
        //TODO use getter setter property for holders object
        holder.teamImage.setImageResource(team.getTeamImage());
        //TODO use getter setter property for holders object
        holder.teamName.setText(team.getTeamTitle());
        //TODO use getter setter property for holders object
        holder.teamAddress.setText(team.getTeamAddress());

        //TODO use getter setter property for holders object
        //TODO on click should be handled on the view holder
        holder.teamRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positionItem = holder.getAdapterPosition();
                context.startActivity(new Intent(context
                        , DetailActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    static class TeamViewHolder extends RecyclerView.ViewHolder {

        //TODO why is context final?? do you know what final means?? don't use things you don't know what are for
        //TODO use private modifiers
        final TextView teamName, teamAddress;
        //TODO why is context final?? do you know what final means?? don't use things you don't know what are for
        //TODO use private modifiers
        final ImageView teamImage;
        //TODO why is context final?? do you know what final means?? don't use things you don't know what are for
        //TODO use private modifiers
        final LinearLayout teamRow;

        TeamViewHolder(View itemView) {
            super(itemView);
            teamName = (TextView) itemView.findViewById(R.id.tituloEquipo);
            teamAddress = (TextView) itemView.findViewById(R.id.direccionEquipo);
            teamImage = (ImageView) itemView.findViewById(R.id.imagenEquipo);
            //TODO don't use this keyword is unnecessary
            this.teamRow = (LinearLayout) itemView.findViewById(R.id.filaEquipo);
        }

        //TODO use getter setter property for objects on this class
    }

}
