package com.tareaapplaudostudios.ui.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

import com.tareaapplaudostudios.R;
import com.tareaapplaudostudios.ui.fragment.TeamListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.contenedor) != null) {
            if (savedInstanceState != null) {
                return;
            }

            //TODO use get instance pattern on your fragments
            //TODO inline unnecessary objects
            TeamListFragment fragment = new TeamListFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.contenedor, fragment).commit();
        }
    }

    // TODO remove this method add action bar logic on create
    public void hideUpButton() {
        ActionBar actionBar = getSupportActionBar();
        //TODO why are you using assert?? do you want your app to crash when actionbar == null ??
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    //TODO why are you doing this ??
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //TODO this is not necessary rethink your logic
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //TODO use descriptive object names
        SubMenu sm = menu.addSubMenu(R.string.compartir)
                .setIcon(android.R.drawable.ic_menu_share);

        // TODO this must be handled from xml
        sm.clearHeader();
        sm.add(0, 1, 1, R.string.facebook).setIcon(R.drawable.ic_icon_facebook);
        sm.add(0, 2, 2, R.string.instagram).setIcon(R.drawable.ic_icon_instagram);
        sm.add(0, 3, 3, R.string.google_plus).setIcon(R.drawable.ic_icon_google_plus);
        sm.add(0, 4, 4, R.string.twitter).setIcon(R.drawable.ic_icon_twitter);

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case 1:
                newCustomTab(1);
                break;
            case 2:
                newCustomTab(2);
                break;
            case 3:
                newCustomTab(3);
                break;
            case 4:
                newCustomTab(4);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //TODO use java doc to document your methods
    //TODO this method is not necessary rethink your logic
    private void newCustomTab(int option) {
        String url = "https://";
        switch (option) {
            case 1:
                url += "facebook.com";
                break;
            case 2:
                url += "instagram.com";
                break;
            case 3:
                url += "plus.google.com";
                break;
            case 4:
                url += "twitter.com";
                break;
        }

        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(this, Uri.parse(url));
    }

}