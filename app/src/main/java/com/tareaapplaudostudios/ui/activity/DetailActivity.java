package com.tareaapplaudostudios.ui.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

import com.tareaapplaudostudios.R;
import com.tareaapplaudostudios.ui.fragment.DetailFragment;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ActionBar actionBar = getSupportActionBar();
        //TODO why are you doing this?? do you want your application to crash if action bar == null??
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        if (findViewById(R.id.details_frag) != null) {
            if (savedInstanceState != null) {
                return;
            }

            DetailFragment detailFragment = new DetailFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.details_frag, detailFragment)
                    .commit();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        //TODO WHY ARE YOU REPLACING THE FRAGMENT AGAIN ON ON CREATE ??  THIS IS A MEMORY LEAK , RETHINK YOUR LOGIC
        DetailFragment detailFragment = new DetailFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contenedor_detalles, detailFragment)
                .commit();
    }

    //TODO THIS IS A CRASH WHY ARE YOU CALLING RECURSIVELY THE SAME METHOD?? THIS IS UNACCEPTABLE
    @Override
    public void onBackPressed() {
        onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //TODO USE DESCRIPTIVE OBJECT NAMES
        SubMenu sm = menu.addSubMenu(R.string.compartir)
                .setIcon(android.R.drawable.ic_menu_share);
        //TODO WHY ARE YOU CLEARING THE MENU?? DON'T USE THING YOU DON'T KNOW WHAT ARE FOR
        sm.clearHeader();
        //TODO THIS MUST BE HANDLED FROM XML
        sm.add(0, 1, 1, R.string.facebook).setIcon(R.drawable.ic_icon_facebook);
        sm.add(0, 2, 2, R.string.instagram).setIcon(R.drawable.ic_icon_instagram);
        sm.add(0, 3, 3, R.string.google_plus).setIcon(R.drawable.ic_icon_google_plus);
        sm.add(0, 4, 4, R.string.twitter).setIcon(R.drawable.ic_icon_twitter);

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case 1:
                newCustomTab(1);
                break;
            case 2:
                newCustomTab(2);
                break;
            case 3:
                newCustomTab(3);
                break;
            case 4:
                newCustomTab(4);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //TODO THIS METHOD IS UNNECESSARY RETHINK YOUR LOGIC
    private void newCustomTab(int option) {
        String url = "https://";
        switch (option) {
            case 1:
                url += "facebook.com";
                break;
            case 2:
                url += "instagram.com";
                break;
            case 3:
                url += "plus.google.com";
                break;
            case 4:
                url += "twitter.com";
                break;
        }

        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(this, Uri.parse(url));
    }

}
